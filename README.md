Msbit SmartAdmin on ReactJS 16.6.3
======================

All the component that include in the router as a Route,
will be devide to a groups in the src/view folder, when each group has an index.js file that do a routes array for this component group (see src/view/example/index.js)

The routing for the Auth-Components will be in the App component
The routing for all the src/view components  will be in the src/layout/components/Layout.js component.

the routes.js file include :
1. an array for all the routing in the view folder for each group.
2. an array for all the auth-components that in the vies folder.

Style: bootstrap 3 & css files

All the file you will add will be in TypeScript/Scss

for translate add your strings to the jsons in the public/assets/api/langs
he.json - json for Hebrew
us.json - json for English

the component that responsible to translate your strings is <Msg phrase={string-to-translate}/>