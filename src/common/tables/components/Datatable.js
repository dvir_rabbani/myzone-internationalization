import React from "react";
import $ from "jquery";
import 'datatables.net-buttons'

require("datatables.net-bs");
require("datatables.net-buttons-bs");
require("datatables.net-buttons/js/buttons.colVis.js");
require("datatables.net-buttons/js/buttons.flash.js");
require("datatables.net-buttons/js/buttons.html5.js");
require("datatables.net-buttons/js/buttons.print.js");
require("datatables.net-colreorder-bs");
require("datatables.net-responsive-bs");
require("datatables.net-select-bs");

export default class Datatable extends React.Component {
  componentDidMount() {
    $.fn.dataTable.ext.errMode = 'throw';
    this.datatable(this.props.data);
  }

  datatable() {
    const element = $(this.refs.table);
    let { options } = { ...this.props } || {};

    let toolbar = "";
    if (options.buttons) toolbar += "B";
    if (this.props.paginationLength) toolbar += "l";
    if (this.props.columnsHide) toolbar += "C";

    if (typeof options.ajax === "string") {
      let url = options.ajax;
      options.ajax = {
        url: url,
        type: 'GET',
        //yo add headers to the ajax request to the table
        // beforeSend: function (request) {
        //   // the token on the headers
        //   request.setRequestHeader('Authorization', `Bearer ${TOKEN}`);
        // }
      };

    }

    options = {
      ...options,
      ...{
        dom:
          "<'dt-toolbar'<'col-xs-12 col-sm-6 text-left-data-table'f><'col-sm-6 col-xs-12 text-right-data-table text-right'" +
          toolbar +
          ">r>" +
          "t" +
          "<'dt-toolbar-footer'<'col-12'p>>",
        oLanguage: {
          sSearch:
            "<span class='input-group-addon input-sm'><i class='glyphicon glyphicon-search'></i></span> ",
          sLengthMenu: "_MENU_"
        },
        autoWidth: false,
        retrieve: true,
        responsive: true
      }
    };

    const _dataTable = element.DataTable(options);

    if (this.props.filter) {
      // Apply the filter
      element.on("keyup change", "thead th input[type=text]", function () {
        _dataTable
          .column(
            $(this)
              .parent()
              .index() + ":visible"
          )
          .search(this.value)
          .draw();
      });
    }

    if (!toolbar) {
      element
        .parent()
        .find(".dt-toolbar")
        .append(
          '<div class="text-right"></div>'
        );
    }

    if (this.props.detailsFormat) {
      const format = this.props.detailsFormat;
      element.on("click", "td.details-control", function () {
        const tr = $(this).closest("tr");
        const row = _dataTable.row(tr);
        if (row.child.isShown()) {
          row.child.hide();
          tr.removeClass("shown");
        } else {
          row.child(format(row.data())).show();
          tr.addClass("shown");
        }
      });
    }
  }

  render() {
    let {
      children,
      options,
      detailsFormat,
      paginationLength,
      ...props
    } = this.props;
    return (
      // <div className="datatable-responsive-wrapper">
      <table {...props} ref="table" id="data-table" >
        {children}
      </table>
      // </div>
    );
  }
}
