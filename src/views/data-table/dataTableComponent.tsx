import React, { useEffect } from 'react'
import { JarvisWidget, WidgetGrid } from '../../common';
import Datatable from '../../common/tables/components/Datatable';
import './dataTableComponent.scss';
import $ from "jquery";
import { connect } from "react-redux";
import { Msg } from '../../common/i18n';
interface IDataTableComponentProps {
    // the names of the data from the respone json
    columns: string[];
    // the title each columns that hold your data from the respone json
    thTitle: string[];
    // the url for the "GET" request to get the data for the table
    jsonDataTableUrl: string;
    amountPerPage: number;
    tableBordered: boolean;
    // if there will be a toggle button to the widget to toggle the table
    toggleBtnWidjet: boolean;
    // title of the widget - table name
    title: string;
    // if you want a button other than the default table butttons
    buttonName?: string;
    // addBtnModalId?: string;
    // if you want to add an "Edit/Delete" buttons for each row in the table
    editBtn?: boolean;
    deleteBtn?: boolean;
    // function for the delete button if you added one
    deleteItem?: (id: string | number) => void;
    // setId to the element id when the user click on the edit button of this element
    setId?: React.Dispatch<React.SetStateAction<number | string>>;
}

const DataTableComponent: React.FC<IDataTableComponentProps> = (props) => {

    useEffect(() => {
        // add onClick to the edit item button
        $('#data-table tbody').on('click', '#editTableBtn', function () {
            //the attribute data-edit hold the id of the item
            const itemID = $(this)[0].attributes['data-edit'].value;
            props.setId && props.setId(itemID);
        });

        // add onClick to the delete item button
        $('#data-table tbody').on('click', '#deleteTableBtn', function () {
            //the attribute data-delete hold the id of the item
            const itemID = $(this)[0].attributes['data-delete'].value;
            props.deleteItem && props.deleteItem(itemID);
        });
    }, [])

    const createColumnsArray = () => {
        const columnsArray = [];
        for (let i = 0; i < props.columns.length; i++) {
            // the column of the "id" will be hidden - visible: false
            if (props.columns[i] === "id") columnsArray.push({ data: null, visible: false });
            else columnsArray.push({ data: props.columns[i] });
        }
        // add a column for each row with Edit button if props.editBtn=true
        if (props.editBtn) {
            columnsArray.push({
                data: null, render: function (data: string, type: string, row: any, meta: any) {
                    //the attribute data-edit hold the id of the item
                    data = '<button data-edit="' + row.id + '" data-toggle="modal" data-target="#' + row.id + '" class="btn btn-default" id="editTableBtn">Edit</button>';
                    return data;
                }
            });
        }
        // add a column for each row with Delete button if props.deleteBtn=true
        if (props.deleteBtn) {
            columnsArray.push({
                data: null, render: function (data: string, type: string, row: any, meta: any) {
                    //the attribute data-delete hold the id of the item
                    data = '<button data-delete="' + row.id + '" class="btn btn-default" id="deleteTableBtn">Delete</button>';
                    return data;
                }
            });
        }
        return columnsArray;
    }

    const createButton = () => {
        // create print, excel, pdf buttons
        //create a reload button with display-none, and when we want to relaod the table we add trigger 'click' to this button(with the function "reloadDatatable()" in common/consts/functions)
        let buttonsArr: any = [
            { extend: "print", text: "Print", className: "btn btn-default buttons-print" },
            { extend: "excel", text: "Excel", className: "btn btn-default buttons-excel" },
            { extend: "pdf", text: "PDF", className: "btn btn-default buttons-pdf" },
            {
                text: "reload",
                attr: { id: 'reload-button' },
                className: "display-none",
                action: function (e: any, dt: { ajax: { url: (arg0: string) => void; reload: () => void; }; }, node: any, config: any) {
                    dt.ajax.url(props.jsonDataTableUrl);
                    dt.ajax.reload();
                }
            }]
        // add your own button with the props.buttonName if you send it props
        if (props.buttonName) buttonsArr.push({ text: props.buttonName, className: "btn btn-primary", attr: { title: "Add", id: 'add-button' } });
        return buttonsArr;
    }

    return (
        <WidgetGrid>
            <div className="row">
                <article className="col-sm-12">
                    <JarvisWidget id="wid-id-2" editbutton={false} color="blueDark" togglebutton={props.toggleBtnWidjet}>
                        <header>
                            <span className="widget-icon">
                                <i className="fa fa-table" />
                            </span>
                            <h2><Msg phrase={props.title} /></h2>
                        </header>
                        <div>
                            <div className="widget-body no-padding data-table-pop-up" id="myTable">
                                <Datatable
                                    options={{
                                        iDisplayLength: props.amountPerPage,
                                        ajax: props.jsonDataTableUrl,
                                        columns: createColumnsArray(),
                                        buttons: createButton()
                                    }}
                                    className={`table table-striped table-hover responsive display nowrap collapsed ${props.tableBordered && 'table-bordered'}`}
                                    width="80%"
                                >
                                    <thead>
                                        <tr>
                                            {props.thTitle.map((item:string, i) => {
                                                return (
                                                    <th key={item + i}>
                                                        <Msg phrase={item} />
                                                    </th>
                                                )
                                            })}
                                        </tr>
                                    </thead>
                                </Datatable>
                            </div>
                        </div>
                    </JarvisWidget >
                </article>
            </div>
        </WidgetGrid>
    )
}

const mapStateToProps = (state: { i18n: any; }) => state.i18n;

export default connect(mapStateToProps)(DataTableComponent);