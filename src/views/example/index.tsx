import Loadable from "react-loadable";
import { Loading } from "../../common/navigation";

const DatatableExample = Loadable({
  loader: () => import("./datatable-page-example/datatableExample"),
  loading: Loading
});

const GraphExample = Loadable({
  loader: () => import("./graphs-page-example/graphsPageExample"),
  loading: Loading
});

const ModalExample = Loadable({
  loader: () => import("./modal-page-example/modalPageExample"),
  loading: Loading
});

const InternationalizationDataTableExamplePage = Loadable({
  loader: () => import("./internationalization-table-example/internationalizationDataTableExamplePage"),
  loading: Loading
});


export const routes = [
  {
    path: "/",
    exact: true,
    component: DatatableExample,
    name: "datatableExample"
  },
  {
    path: "/graphs-example",
    exact: true,
    component: GraphExample,
    name: "graphsPageExample"
  },
  {
    path: "/modal-example",
    exact: true,
    component: ModalExample,
    name: "modalPageExample"
  },
  {
    path: "/internationalization",
    exact: true,
    component: InternationalizationDataTableExamplePage,
    name: "internationalization"
  },  
];