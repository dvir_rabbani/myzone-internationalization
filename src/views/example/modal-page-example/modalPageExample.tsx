import React from 'react'
import { BigBreadcrumbs, Stats } from '../../../common';
import PopUpModal from '../../modal/modal';
import PercentageWheel from '../../precentageWheelPie/percentageWheel';
import { statsLayoutData } from "./jsons/statsLayoutArray";
import './modalPageExample.scss';

const ModalPage: React.FC = () => {
  const dataTargetModal = "btnOpenModalExample";
  return (
    <>
      <div className="msbit-crumb-state">
        <BigBreadcrumbs
          items={["Examples", "Modal"]}
          icon={"fa"}
          className=""
        />
        <Stats statsData={statsLayoutData} />
      </div>
      <div className="modal-page-content">
        <div className="display-flex align-center">
          <h1>Modal Example</h1>
          <PercentageWheel modeColor={"msbityellow"} pieSize={"60"} />
        </div>
        <button className="btn btn-primary" data-toggle="modal" data-target={`#${dataTargetModal}`}>open Modal</button>
      </div>
      <PopUpModal
        btnModalId={dataTargetModal}
        title={"Modal Example Title"}
        children={
          <>
            <h2>Modal Example</h2>
            <h4>You can put any content {"&"} component you want here</h4>
          </>
        }
      />
    </>
  )
}
export default ModalPage;