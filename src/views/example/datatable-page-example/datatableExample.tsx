import React, { useState } from "react";
import { BigBreadcrumbs, Stats } from "../../../common";
import { addAlertMessage, addConfirmMessageQ, reloadDatatable } from "../../../common/consts/functions";
import DataTableComponent from "../../data-table/dataTableComponent";
import { statsLayoutData } from "./jsons/statsLayoutArray";
import "./datatableExample.scss";

const DataTableExamplePage: React.FC = () => {
  const [userId, setUserId] = useState<number | string>(0);

  const deleteUserConfirm = async (userId: number | string) => {
    addConfirmMessageQ("Are you sure you want to delete this user?");
    let yesButtonFromConfirmAlert = $('#divSmallBoxes .textoFoto button#yesConfirmDel');
    // when the 'yes' is click add function delete
    console.log("userId: ", userId);
    yesButtonFromConfirmAlert.on('click', () => { deleteUser(userId) });
  }

  const deleteUser = async (userId: number | string) => {
    //the request to delete user by his id, if success alert success and reload(update) the table:
    reloadDatatable();
    addAlertMessage("Success", `user is deleted`, "success");
  }

  return (
    <div>
      <div className="msbit-crumb-state">
        <BigBreadcrumbs
          items={["Examples","DataTable"]}
          icon={"fa"}
          className=""
        />
        <Stats statsData={statsLayoutData} />
      </div>
      <div className="datatable-responsive list-table">
        <DataTableComponent
          amountPerPage={5}
          jsonDataTableUrl={"assets/api/tables/datatables.users.json"}
          columns={["id", "userName", "firstName", "lastName", "email", "phone", "role"]}
          thTitle={["ID",  "User Name", "First Name", "Last Name", "Email", "Phone", "Role"]}
          tableBordered={false}
          toggleBtnWidjet={true}
          title={"Users"}
          buttonName={"Add User"}
          setId={setUserId}
          deleteItem={deleteUserConfirm}
          editBtn={true}
          deleteBtn={true}
        />
      </div>
    </div>
  );
}
export default DataTableExamplePage;