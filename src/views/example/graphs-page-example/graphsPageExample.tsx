import React, { useState } from 'react'
import { BigBreadcrumbs, Stats, WidgetGrid } from '../../../common';
import SingleDoughnutChart from '../../single-chart/singleDoughnutChart';
import SingleLineChart from '../../single-chart/singleLineChart';
//jsons
import { statsLayoutData } from "./graphs-jsons/statsLayoutArray";
import issuesByType from './graphs-jsons/issuesByType.json';
import issuesByTypeLine from './graphs-jsons/issueByTypeTrendLine.json';
// style the charts container
import "../../single-chart/style-charts/chartsStayle.scss"

const GraphsPage: React.FC = () => {
  return (
    <>
      <div className="msbit-crumb-state">
        <BigBreadcrumbs
          items={["Examples", "Graphs"]}
          icon={"fa fa-bar-chart-o"}
          className=""
        />
        <Stats statsData={statsLayoutData} />
      </div>
      <WidgetGrid>
        <div className="charts-container">
          <article className="col-xs-12 col-sm-6 col-md-6 col-lg-6">
          <SingleLineChart chartData={issuesByTypeLine} title={"Line Chart Name"} />
          </article>
          <article className="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <SingleDoughnutChart dataChartJson={issuesByType} title={"Doughnut Chart Name"}/>
          </article>
        </div>
      </WidgetGrid>

    </>
  )
}
export default GraphsPage;