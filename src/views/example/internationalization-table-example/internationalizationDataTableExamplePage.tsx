import React, { useState } from "react";
import { BigBreadcrumbs } from "../../../common";
import { SERVER_API_FILE } from "./consts/url";
import { addAlertMessage, addConfirmMessageQ, reloadDatatable } from "../../../common/consts/functions";
import DataTableComponent from "./componenets/data-table/dataTableComponent";
import $ from "jquery"
import PopUpModalForm from "./componenets/popUpModalForm/popUpModalForm";
import "./internationalizationDataTableExamplePage.scss";

const InternationalizationDataTableExamplePage: React.FC = () => {
  const [pageId, setPageId] = useState<number | string>(-1);
  const dataTargetOpenModalPageContentForm = "OpenModalPageContentForm";
  const deleteWidgetConfirm = async (pageId: number | string) => {
    addConfirmMessageQ("Are you sure you want to delete this widget?");
    let yesButtonFromConfirmAlert = $('#divSmallBoxes .textoFoto button#yesConfirmDel');
    // when the 'yes' is click add function delete
    yesButtonFromConfirmAlert.on('click', () => { 
      deleteWidget(pageId);
      // reload table 
      reloadDatatable();
    });
  }

  const deleteWidget = async (pageId: number | string) => {
    //the request to delete user by his id, if success alert success and reload(update) the table:
    let url = SERVER_API_FILE + "/" + pageId.toString();
    let resp = await fetch(url, {
      method: 'DELETE',
      headers: {
        'Accept': 'application/json',
      },
    });
    let data = await resp.json();
    
    if (data.id) {
      addAlertMessage("Success", `widget is deleted`, "success");
    } 
  }


  return (
    <div>
      <div className="msbit-crumb-state">
        <BigBreadcrumbs
          items={["Pages", "Content"]}
          icon={"fa"}
        />
      </div>
      <div className="datatable-responsive list-table">
        <DataTableComponent
          amountPerPage={5}
          jsonDataTableUrl={SERVER_API_FILE}
          columns={["id", "page", "language"]}
          thTitle={["ID", "Page", "Language"]}
          tableBordered={false}
          toggleBtnWidjet={true}
          title={"Users"}
          buttonName={"Add Page"}
          setId={setPageId}
          deleteItem={deleteWidgetConfirm}
          editBtn={true}
          deleteBtn={true}
        />
      </div>
        <PopUpModalForm
        btnModalId={dataTargetOpenModalPageContentForm}
        title={"page Content Form"}
        id={pageId}
      /> 
    </div>
  );
}
export default InternationalizationDataTableExamplePage;