import React, { useEffect, useState } from "react";
import { Msg } from "../../../../../common/i18n";
import { SERVER_API_FILE } from "../../consts/url";
import { FormData } from "../../interface/formData.interface";
import PageContentForm from "../pageContentForm/pageContentForm";
import $ from "jquery";

// modal component
// - the modal body include the children you add into it - generic component
// - each button that need to open the modal with his props.children has to contain: 
// 1. data-toggle: 'modal'
// 2. data-target: '#'+props.btnModalId -> Which is transferred to the component (the id of the modal you want to be)

interface PopUpModalProps {
  title: string;
  btnModalId: string;
  id?: number | string;
}

// the modal consist an a form which can add\edit based on the id, if the id is -1 is an add form else edit form
const PopUpModalForm: React.FC<PopUpModalProps> = ({ btnModalId, title, id }) => {
  const [formData, setFormData] = useState<FormData>(null);
  const [isLoading, setIsLoading] = useState<boolean>(true);

  // default values for Add form 
  const defaultValuesInit = {
    page: "",
    language: [
      {
        code: "en",
        direction: "ltr",
        title: "",
        header: "",
        body: "",
        footer: "",
        button: []
      }
    ]
  };

  // get data for edit form 
  const getFormData = async () => {
    setIsLoading(true);
    let url = SERVER_API_FILE + '/' + id;
    let resp = await fetch(url);
    let data = await resp.json();
    setFormData(data);
    setIsLoading(false);
  }

  useEffect(() => {
    if (id !== -1) {
      getFormData();
    }
  }, [id]);


  return (
    <div
      className="modal fade"
      id={btnModalId}
      tabIndex={-1}
      role="dialog"
      aria-labelledby="myModalLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog">
        <div className="modal-content">
          <div className="modal-header">
            <button
              type="button"
              className="close btn-close-modal"
              data-dismiss="modal"
              aria-hidden="true"
              aria-label="close"
              // "form_hook_reset_btn" is a button with which have a css with "display:none" just for reset the form 
              onClick={() => { $("#form_hook_reset_btn").trigger('click') }}
            >
              &times;
            </button>
            <h4 className="modal-title" id="myModalLabel">
              <Msg phrase={title} />
            </h4>
          </div>
          <div className="modal-body">
            {/* your children props */}
            <div>
              {id === -1 && <PageContentForm data={{ ...defaultValuesInit }} />}
              {id !== -1 && !isLoading && formData && <PageContentForm data={formData} />}
            </div>
          </div>
          <div className="modal-footer">
            <button
              type="button"
              className="btn btn-default btn-close-modal"
              data-dismiss="modal"
              onClick={() => { $("#form_hook_reset_btn").trigger('click') }}
            >Cancel</button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default PopUpModalForm;