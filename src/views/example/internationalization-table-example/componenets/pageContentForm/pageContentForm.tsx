import React from "react";
import { useForm } from "react-hook-form";
import FieldArray from "./fieldArray";
import { addAlertMessage, closeModalPopup, reloadDatatable } from "../../../../../common/consts/functions";
import { SERVER_API_FILE } from "../../consts/url";
import "./pageContentForm.scss";


export default function PageContentForm({ data }) {

    let defaultValues = data;
    console.log(defaultValues);

    const { control, register, handleSubmit, getValues, formState: { errors, isValid }, reset } = useForm({
        defaultValues: data
    });

    const onSubmit = async (formData) => {
        if (isValid) {
            await closeModalPopup(".close.btn-close-modal");
            await updateData(formData);
            await addAlertMessage("Success", `page is ${formData.id ? 'updated' : "added"}`, "success");
            await reloadDatatable();
        } else {
            return false;
        }
    }

    const updateData = async (formData) => {
        let url = SERVER_API_FILE + `/${formData.id ? '/' + formData.id : ''}`;
        let resp = await fetch(url, {
            method: formData.id ? 'PUT' : 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(formData)

        });
        let data = await resp.json();
        return data;
    }

    return (
        <form
            id="pageAddEditForm"
            className="form-horizontal"
            onSubmit={handleSubmit(onSubmit)}>
            <fieldset>
                <div className="form-group">
                    <label htmlFor="page_name" className="col-md-3 control-label">*Page</label>
                    <div className="col-md-6">
                        <input
                            type="text"
                            id="page_name"
                            className="form-control"
                            {...register("page", { required: true, maxLength:30 })} />
                        {errors.page && errors.page.type === "required" && <span className="text-danger">field is required</span>}
                        {errors.page && errors.page.type === "maxLength" && <span className="text-danger">max length exceeded</span>}
                    </div>
                </div>
            </fieldset>
            <FieldArray {...{ control, register, defaultValues, getValues, errors, reset }} />
            <div className="form-actions">
                <div className="row">
                    <div className="col-md-6">
                        <button
                            className="dt-button btn btn-primary"
                            id='btn_submit'
                            tabIndex={0} aria-controls="data-table"
                            title="Add"
                            type='submit'
                        >
                            <span>submit</span>
                        </button>
                    </div>
                </div>
            </div>
            <button type="button" onClick={() => reset()} id="form_hook_reset_btn" style={{ display: "none" }}>reset</button>
        </form>
    );
}
