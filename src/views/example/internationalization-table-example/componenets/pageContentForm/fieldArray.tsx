import React, { useState } from "react";
import { useFieldArray } from "react-hook-form";
import NestedArray from "./nestedFieldArray";


export default function Fields({
  getValues,
  control,
  register,
  defaultValues,
  errors,
  reset
}) {
  const { fields, append, remove } = useFieldArray({
    control,
    name: "language"
  });
  const [childArray, setChildArray] = useState();


  const checkFieldError = (index, fieldName, errorType: string) => {
    if ((errors && errors.language && errors.language[index] && errors.language[index][fieldName])) {
      let fieldErrors = errors.language[index][fieldName];
      if (fieldErrors.type === errorType) {
        return true;

      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  return (
    <>
      <div className="form-group">
        <div className="col-md-3"></div>
        <div className="col-md-6">
          <button
            className="dt-button btn btn-info"
            tabIndex={0} aria-controls="data-table"
            type="button"
            onClick={() => {
              setChildArray(undefined);
              append({
                code: "en",
                direction: "ltr",
                title: "",
                header: "",
                body: "",
                footer: "",
                button: []
              });
            }}
          >
            <span>Add Language</span>
          </button>
        </div>
      </div>

      {fields.map((item, index) => {
        return (
          <fieldset key={item.id}>
            <legend className="fieldset_language_legend">
              <span>language</span>
              {
                index !== 0 ?
                  <span className="remove_field_icon"
                    onClick={(e) => remove(index)}
                  ><i className="fa fa-times text-danger"></i></span>
                  : null
              }
            </legend>
            <div className="form-group">
              <label htmlFor="code" className="col-md-3 control-label">Code</label>
              <div className="col-md-6">
                <select className="form-control" id="page_code"
                  {...register(`language[${index}].code`)}
                >
                  <option value="en">English</option>
                  <option value="he">Hebrew</option>
                  <option value="es">Español</option>
                </select>
              </div>
            </div>
            <div className="form-group">
              <label htmlFor='direction' className="col-md-3 control-label">Direction</label>
              <div className="col-md-6">
                <div className="radio">
                  <div className="radio">
                    <label>
                      <input type="radio" value="ltr"
                        {...register(`language[${index}].direction`)}
                      />
                      ltr
                    </label>
                  </div>
                  <label>
                    <input type="radio" value="rtl"
                      {...register(`language[${index}].direction`)} />
                    rtl
                  </label>
                </div>
              </div>
            </div>
            <div className="form-group">
              <label htmlFor='title' className="col-md-3 control-label">*Title</label>
              <div className="col-md-6">
                <input type="text" className="form-control" id='title'
                  {...register(`language[${index}].title`, { required: true, maxLength: 30 })}
                />
                {checkFieldError(index, "title", "required") && <span className="text-danger">field is required</span>}
                {checkFieldError(index, "title", "maxLength") && <span className="text-danger">max length exceeds</span>}
              </div>
            </div>
            <div className="form-group">
              <label htmlFor='page_header' className="col-md-3 control-label">Header</label>
              <div className="col-md-6">
                <input type="text" className="form-control" id='page_header'
                  {...register(`language[${index}].header`, { maxLength: 30 })}
                />
                {checkFieldError(index, "header", "maxLength") && <span className="text-danger">max length exceeds</span>}
              </div>
            </div>
            <div className="form-group">
              <label htmlFor='page_body' className="col-md-3 control-label">Body</label>
              <div className="col-md-6">
                <input type="text" className="form-control" id='page_body'
                  {...register(`language[${index}].body`)}
                />
              </div>
            </div>
            <div className="form-group">
              <label htmlFor='page_footer' className="col-md-3 control-label">Footer</label>
              <div className="col-md-6">
                <input type="text" className="form-control" id='page_footer'
                  {...register(`language[${index}].footer`, { maxLength: 30 })}
                />
                {checkFieldError(index, "footer", "maxLength") && <span className="text-danger">max length exceeds</span>}
              </div>
            </div>
            <fieldset>
              <NestedArray
                childArray={childArray}
                nestIndex={index}
                {...{ control, register, errors }}
              />
            </fieldset>
          </fieldset>
        );
      })}
    </>
  );
}