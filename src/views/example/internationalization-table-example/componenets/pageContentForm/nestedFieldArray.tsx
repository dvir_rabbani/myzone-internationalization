import React, { useEffect } from "react";
import { useFieldArray } from "react-hook-form";

export default ({ nestIndex, control, register, errors, childArray }) => {
  const { fields, remove, append } = useFieldArray({
    control,
    name: `language[${nestIndex}].button`
  });

  useEffect(() => {
    if (childArray) {
      append(childArray);
    }
  }, [append]);

  const addBtnLabel = () => {
    append({
      label: "",
    })
  }

  const checkFieldError = (nestIndex, index, errorType: string) => {
    if (errors && errors.language && errors.language[nestIndex] && errors.language[nestIndex].button && errors.language[nestIndex].button[index] && errors.language[nestIndex].button[index].label) {
      let fieldErrors = errors.language[nestIndex].button[index].label;
      if (fieldErrors.type === errorType) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  return (
    <div>
      <div className="form-group">
        <label htmlFor='add_button_label' className="col-md-3 control-label">Labels</label>
        <div id='add_button_label' className="col-md-6">
          <button
            className="dt-button btn btn-info"
            tabIndex={0} aria-controls="data-table"
            title="Add"
            type="button"
            onClick={addBtnLabel}
          >
            <span>Add buttons label</span>
          </button>
        </div>
      </div>

      {
        fields.map((item, buttonLabelIndex) => {
          return (

            <div key={item.id} className="form-group">
              <label htmlFor={`buttonLabel-${buttonLabelIndex}`} className="col-md-3 control-label justify-content-between">Label</label>
              <div className="col-md-6">
                <input type="text" className="form-control" name="label"
                  id={`buttonLabel-${buttonLabelIndex}`}
                  {...register(`language[${nestIndex}].button[${buttonLabelIndex}].label`, { maxLength: 30 })}
                />
                {checkFieldError(nestIndex, buttonLabelIndex, "maxLength") && <span className="text-danger">max length exceeds</span>}
              </div>
              <div className="col-md-3">
                <span className="remove_field_icon" onClick={() => remove(buttonLabelIndex)} >
                  <i className="fa fa-minus text-danger"></i>
                </span>
              </div>
            </div>
          );
        })
      }
    </div >
  );
};
