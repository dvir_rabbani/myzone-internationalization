export interface FormData {
    page: string,
    language: Language[],

}
export interface Language {
    code: string,
    direction: string,
    title: string,
    header: string,
    body: string,
    footer: string,
    button?: ButtonLabelType[]
}

type ButtonLabelType = { label: string }