export const statsLayoutData = [
    {
        title: "success",
        total: "159",
        values: [
            1300,
            1877,
            2500,
            2577,
            2100,
            3000,
            2000
        ],
        color: "greenDark",
        icon:"fa fa-arrow-circle-up"
    },
    
    {
        title: "population",
        total: "4,500",
        values: [
            110, 
            150,
            300,
            190,
            400,
            240
        ],
        color: "blue",
        icon:""
    },
    {
        title: "information",
        total: "2,587",
        values: [
            110,
            150,
            300,
            130,
            400,
            240,
            220,
            310
        ],
        color: "purple",
        icon:""
    }
]