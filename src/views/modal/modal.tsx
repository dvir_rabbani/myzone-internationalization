import React, { ReactChild, ReactChildren } from "react";
import { Msg } from "../../common/i18n";

// modal component
// - the modal body include the children you add into it - generic component
// - each button that need to open the modal with his props.children has to contain: 
// 1. data-toggle: 'modal'
// 2. data-target: '#'+props.btnModalId -> Which is transferred to the component (the id of the modal you want to be)

interface PopUpModalProps {
  title: string;
  children: ReactChild | ReactChildren;
  btnModalId: string;
}

const PopUpModal: React.FC<PopUpModalProps> = ({ btnModalId, title, children, }) => {

  
  return (
    <div
      className="modal fade"
      id={btnModalId}
      tabIndex={-1}
      role="dialog"
      aria-labelledby="myModalLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog">
        <div className="modal-content">
          <div className="modal-header">
            <button
              type="button"
              className="close btn-close-modal"
              data-dismiss="modal"
              aria-hidden="true"
              aria-label="close"
            >
              &times;
            </button>
            <h4 className="modal-title" id="myModalLabel">
              <Msg phrase={title} />
            </h4>
          </div>
          <div className="modal-body">
            {/* your children props */}
            <div>{children}</div>
            {/* your children props */}
          </div>
          <div className="modal-footer">
            <button
              type="button"
              className="btn btn-default btn-close-modal"
              data-dismiss="modal"
            >Cancel</button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default PopUpModal;