import React from 'react'
import { JarvisWidget } from '../../common';
import { HighchartTable } from '../../common/graphs/highchart';
import { Msg } from '../../common/i18n';
import './style-charts/chartsStayle.scss'


interface ISingleLineChartProps {
    chartData: tableChartData;
    title: string;
}
interface tableChartData {
    labels: string[];
    data: { year: string, yearData: string[] }[];
}

const SingleLineChart: React.FC<ISingleLineChartProps> = (props) => {

    return (
        <div>
            <JarvisWidget id="wid-id-4" editbutton={false} togglebutton={true}>
                <header className="ui-sortable-handle">
                    <span className="widget-icon">
                        <i className="fa fa-line-chart" />
                    </span>
                    <h2><Msg phrase={props.title} /></h2>
                </header>
                <div>
                    <div className="widget-body">
                        <div className="line-chart-table">
                            {/* each chart thete is a table with the data - for now the table with display-none */}
                            <HighchartTable
                                className="highchart table table-hover table-bordered"
                                data-graph-container=".. .. .highchart-container"
                                data-graph-type="line"
                            >
                                <caption><Msg phrase={props.title} /></caption>
                                <thead>
                                    <tr>
                                        <th>year</th>
                                        {props.chartData.labels.map((item: string, i: number) => {
                                            return (
                                                <th key={item}>{item}</th>
                                            )
                                        })}
                                    </tr>
                                </thead>
                                <tbody>
                                    {props.chartData.data.map((item: { year: string, yearData: string[] }) => {
                                        return (
                                            <tr key={item.year}>
                                                <td>{item.year}</td>
                                                {item.yearData.map((item: string, j: number) => {
                                                    return (
                                                        <td key={j}>{item}</td>
                                                    )
                                                })}
                                            </tr>
                                        )
                                    })}
                                </tbody>
                            </HighchartTable>
                        </div>
                        {/* the chart */}
                        <div>
                            <div className="highchart-container" />
                        </div>
                    </div>
                </div>
            </JarvisWidget>
        </div>
    )

}
export default SingleLineChart;