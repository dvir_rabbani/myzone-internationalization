import React from 'react'
import { JarvisWidget } from '../../common';
import { IDataDoughnutChartJson } from '../../common/consts/model';
import { ChartJsGraph } from '../../common/graphs/chartjs';
import { Msg } from '../../common/i18n';

interface ISingleDoughnutChartProps {
  dataChartJson: IDataDoughnutChartJson;
  title: string;
}

const SingleDoughnutChart: React.FC<ISingleDoughnutChartProps> = (props) => {

  return (
    <>
      <JarvisWidget id="wid-id-4" editbutton={false}  togglebutton={true}>
        <header className="ui-sortable-handle">
          <span className="widget-icon">
            <i className="fa fa-pie-chart" />
          </span>
          <h2><Msg phrase={props.title} /></h2>
        </header>
        <div>
          <div className="widget-body">
            <ChartJsGraph
              type="doughnut"
              data={props.dataChartJson}
              optionType={"doughnut"}
            />
          </div>
        </div>
      </JarvisWidget>
    </>
  )
}
export default SingleDoughnutChart;