import React from "react";

import UiValidate from "../../../common/forms/validation/UiValidate";

export default class Login extends React.Component {
  onClick = e => {
    e.preventDefault();
  };
  render() {
    return (
      <div id="extr-page">
        <header id="header" className="animated fadeInDown auth-header">
          <div id="logo-group">
            <a href="#/">
              <span id="logo">
                {" "}
                <img src="assets/img/logo-white.png" alt="SmartAdmin" className="img-auth-logo"/>{" "}
              </span>
            </a>
          </div>

          <span id="extr-page-header-space">
            {" "}
            <span className="hidden-mobile hiddex-xs">Need an account?</span>
            &nbsp;
            <a href="#/register" className="btn btn-danger">
              Create account
            </a>{" "}
          </span>
        </header>
        <div id="main" role="main" className="animated fadeInDown">
          <div id="content" className="container">
            <div className="msbit-flex-center">
              <div className="col-xs-12 col-sm-12 col-md-5 col-lg-4">
                <div className="well no-padding">
                  <UiValidate>
                    <form
                      action="#/dashboard"
                      id="login-form"
                      className="smart-form client-form"
                    >
                      <header>Sign In</header>
                      <fieldset>
                        <section>
                          <label className="label">E-mail</label>
                          <label className="input">
                            {" "}
                            <i className="icon-append fa fa-user" />
                            <input
                              type="email"
                              name="email"
                              data-smart-validate-input=""
                              data-required=""
                              data-email=""
                              data-message-required="Please enter your email address"
                              data-message-email="Please enter a VALID email address"
                            />
                            <b className="tooltip tooltip-top-right">
                              <i className="fa fa-user txt-color-teal" />
                              Please enter email address/username
                            </b>
                          </label>
                        </section>
                        <section>
                          <label className="label">Password</label>
                          <label className="input">
                            {" "}
                            <i className="icon-append fa fa-lock" />
                            <input
                              type="password"
                              name="password"
                              data-smart-validate-input=""
                              data-required=""
                              data-minlength="3"
                              data-maxnlength="20"
                              data-message="Please enter your email password"
                            />
                            <b className="tooltip tooltip-top-right">
                              <i className="fa fa-lock txt-color-teal" /> Enter
                              your password
                            </b>{" "}
                          </label>

                          <div className="note">
                            <a href="#/forgot">Forgot password?</a>
                          </div>
                        </section>
                        <section>
                          <label className="checkbox">
                            <input
                              type="checkbox"
                              name="remember"
                              defaultChecked={true}
                            />
                            <i />
                            Stay signed in
                          </label>
                        </section>
                      </fieldset>
                      <footer>
                        <button type="submit" className="btn btn-primary">
                          Sign in
                        </button>
                      </footer>
                    </form>
                  </UiValidate>
                </div>
                <h5 className="text-center"> - Or sign in using -</h5>
                <ul className="list-inline text-center">
                  <li>
                    <a href="#/"
                      onClick={this.onClick}
                      className="btn btn-primary btn-circle"
                    >
                      <i className="fa fa-facebook" />
                    </a>
                  </li>
                  <li>
                    <a href="#/"
                      onClick={this.onClick}
                      className="btn btn-info btn-circle"
                    >
                      <i className="fa fa-twitter" />
                    </a>
                  </li>
                  <li>
                    <a href="#/"
                      onClick={this.onClick}
                      className="btn btn-warning btn-circle"
                    >
                      <i className="fa fa-linkedin" />
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
