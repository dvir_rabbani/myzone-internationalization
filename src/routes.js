import { routes as auth } from "./views/auth";
import { routes as example } from "./views/example";
import { routes as appViews } from "./views/app-views";

export const routes = [
  ...appViews,
  ...example
];

export const authRoutes = [
  ...auth
];
