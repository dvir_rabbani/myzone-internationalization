const fs = require("fs");
const { DB_FILE_PATH } = require("../consts/consts");

exports.WriteTextToFileAsync = async (contentToWrite) => {
  await fs.writeFile(DB_FILE_PATH, contentToWrite, (err) => {
    if (err) {
      console.log(err);
    } else {
      console.log('Done writing to file...');
    }
  })
}
exports.getDataFromJsonFile = () => {
  let _data = fs.readFileSync(DB_FILE_PATH, 'utf8');

  return JSON.parse(_data);
}
