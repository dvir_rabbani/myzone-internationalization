const express = require("express");
const path = require("path");
const http = require("http");
const app = express();
const { routesInit, corsAccessControl } = require("./routes/config_routes");


app.use(express.json());
app.use(express.static(path.join(__dirname, "public")));


corsAccessControl(app);
routesInit(app);
app.get("/", (req, res) => {
  res.send("This is from express.js");
});

const server = http.createServer(app);
let port = process.env.PORT || "5000";
server.listen(port,()=> {
  console.log("server running on port " + port);
});