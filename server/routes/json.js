const express = require("express");
const router = express.Router();
const { WriteTextToFileAsync, getDataFromJsonFile } = require("../utils/files");


router.get("/", async (req, res) => {
  try {
    let data = getDataFromJsonFile();
    return res.json({ data, })
  }
  catch (err) {
    console.log(err);
    res.status(400).json(err);
  }
})

router.get("/:id", async (req, res) => {

  try {
    let id = req.params.id;
    let data = getDataFromJsonFile();
    let item = data.find(item => (item.id === id));
    res.json(item);
  }
  catch (err) {
    console.log(err);
    res.status(400).json(err);
  }
})

router.post('/', async (req, res) => {
  try {
    let item = req.body

    let fileData = getDataFromJsonFile();
    let data = [...fileData, item];
    // get id from last record id plus 10 and convert it back to string 
    item.id = (parseInt(fileData[fileData.length - 1].id) + 10).toString()
    WriteTextToFileAsync(JSON.stringify(data));
    return res.json(item)

  } catch (err) {
    console.log(err);
    res.status(400).json(err);
  }
});

router.put('/:id', async (req, res, next) => {

  try {
    let id = req.params.id;
    let data = getDataFromJsonFile();
    let updatedData = data.map(item => {
      if (item.id === id) {
        return req.body;
      } else {
        return item;
      }
    })
    await WriteTextToFileAsync(JSON.stringify(updatedData));

    res.json(req.body)
  } catch (err) {
    console.log(err);
    res.status(400).json(err);
  }
});

router.delete("/:id", async (req, res) => {

  try {

    let id = req.params.id;
    let data = getDataFromJsonFile();
    let _filter = data.filter(item => item.id !== id);
    await WriteTextToFileAsync(JSON.stringify(_filter));
    return res.json({ id });
  }
  catch (err) {
    console.log(err);
    res.status(400).json(err);
  }
})


module.exports = router;


